/**
 * Represents a Context Handler.
 * @constructor
 **/
function handler(id) {
    this.id = id;
    /**
     Add a new context support to the context handler.
     @param {String} pluginId Plugin Id
     @param {String} contextType Context Type
     @param {object} optParams Optional callback and listener
     @example
     var batteryLevelCallback = function(status, result) {
        switch(status) {
            case Dynamix.Enums.SUCCESS :
                break;
            case Dynamix.Enums.FAILURE :
                break;
            case Dynamix.Enums.INSTALL_PROGRESS :
                break;
            case Dynamix.Enums.WARNING :
                break;
        }
    };
     var batteryLevelListener = function(status, result) {
        switch(status) {
            case Dynamix.Enums.CONTEXT_RESULT :
                batteryLevel = parseInt(result.batteryLevel);
                console.log(result.batteryLevel);
                break;
        }
    };

     dynamixContextHandler.addContextSupport( "org.ambientdynamix.contextplugins.batterylevel",
     "org.ambientdynamix.contextplugins.batterylevel", {callback : batteryLevelCallback , listener : batteryLevelListener, pluginVersion : '2.0.0.1'});
     **/
    this.addContextSupport = function (pluginId, contextType, optParams) {
        try {
            var endpointParamsString = "addContextSupport?contextHandlerId=" + id +
                "&contextType=" + contextType + "&pluginId=" + pluginId;
            if (typeof optParams !== 'undefined') {
                if (typeof optParams.callback !== 'undefined') {
                    var callbackId = Utils.generateGuid();
                    Dynamix.Callbacks[callbackId] = optParams.callback;
                    endpointParamsString = endpointParamsString + "&callbackId=" + callbackId;
                }
                if (typeof optParams.listener !== 'undefined') {
                    var listenerId = Utils.generateGuid();
                    Dynamix.Listeners[listenerId] = optParams.listener;
                    endpointParamsString = endpointParamsString + "&contextListenerId=" + listenerId;
                }
                if (typeof optParams.pluginVersion !== 'undefined') {
                    endpointParamsString = endpointParamsString + "&pluginVersion=" + optParams.pluginVersion
                }
            }
           WebClient.sendToDynamixInstance(endpointParamsString);
        } catch (e) {
            console.log("Add context support failed : " + e);
        }
    };

    /**
     Make a new context request. A context request can only be made if the
     context support request has been made successfully.
     @param {String} pluginId Id of the plugin.
     @param {String} contextType Context type for the context request.
     @param {function} callback a function to which Dynamix would return the context request result object.
     @param {Object} optParams Optional parameters for the request.
     @example
     var voiceControlContextRequestCallback = function(status, result) {
        switch(status) {
            case Dynamix.Enums.SUCCESS:
                doSomethingWithResult(result);
                break;
        }
    };
     dynamixContextHandler.contextRequest("org.ambientdynamix.contextplugins.speechtotext",
     "org.ambientdynamix.contextplugins.speechtotext.voiceresults", voiceControlContextRequestCallback, {pluginVersion : "2.0.1.2"} );
     **/
    this.contextRequest = function (pluginId, contextType, callback, optParams) {
        try {
            var callbackId = Utils.generateGuid();
            Dynamix.Callbacks[callbackId] = callback;
            var endpointParamsString = "contextrequest?contextHandlerId=" + id +
                "&contextType=" + contextType + "&pluginId=" + pluginId +
                "&callbackId=" + callbackId;
            if (typeof optParams !== 'undefined' && typeof optParams.pluginVersion !== 'undefined') {
                endpointParamsString = endpointParamsString + "&pluginVersion=" + optParams.pluginVersion;
            }
            WebClient.sendToDynamixInstance(endpointParamsString);
        } catch (e) {
            console.log("Context request failed : " + e);
        }
    };

    /**
     Make a configured request to the Dynamix Framework.
     Since, the request to be made is similar for addConfiguredContextSupport() and configuredContextRequest()
     we pass on the parameters from these methods to makeConfiguredRequest() which makes the relevant REST Request.

     NOTE : This method is used internally and the web client does not need to use this method.
     The client should make use of addConfiguredContextSupport() and configuredContextRequest() as required.
     @private
     */
    makeConfiguredRequest = function (endpoint, method, pluginId, contextType, optParams) {

        /**
         Converts an object to a string of paramaters which can be appended to a GET URL.
         */
        var getParamStringFromObject = function (obj) {
            var str = [];
            for (var p in obj) {
                if (obj.hasOwnProperty(p)) {
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                }
            }
            return str.join("&");
        };

        var method = method.toUpperCase();
        var endpointParamsString = endpoint + "?contextHandlerId=" + id +
            "&contextType=" + contextType + "&pluginId=" + pluginId;

        if (typeof optParams !== 'undefined') {
            if (typeof optParams.callback !== 'undefined') {
                var callbackId = Utils.generateGuid();
                Dynamix.Callbacks[callbackId] = optParams.callback;
                endpointParamsString = endpointParamsString + "&callbackId=" + callbackId;
            }
            if (typeof optParams.listener !== 'undefined') {
                var listenerId = Utils.generateGuid();
                Dynamix.Listeners[listenerId] = optParams.listener;
                endpointParamsString = endpointParamsString + "&contextListenerId=" + listenerId;
            }
            if (typeof optParams.pluginVersion !== 'undefined') {
                endpointParamsString = endpointParamsString + "&pluginVersion=" + optParams.pluginVersion;
            }
        }

        if (method == "GET" || method == "POST" || method == "PUT" || method == "DELETE") {
            endpointParamsString = endpointParamsString + "&" + getParamStringFromObject(optParams.params);
        } else {
            console.log("Unsupported REST Method");
        }

        var url = WebClient.getFinalUrl(endpointParamsString);
        $.ajax({
            url : url,
            type : method,
            error : function(jqXHR, textStatus, errorThrown){
                console.log("Send to dynamix request failed : " + jqXHR.status);
                if(DEBUG){
                    console.log("Request Failed : " + endpointParamsString);
                }
            }
        });
    };

    /**
     Allows the web clients to make configured context requests. These requests
     can only be made if a context support has been successfully requested.
     @param {String} method The Dynamix Framework supports GET, PUT, POST and DELETE Methods.
     @param {String} pluginId The id of the plugin
     @param {String} contextType The context type.
     @param {Object} optParams The optional parameters.
     @example
     var paramsObject = {color : "red", lux : 22};
     var headerObject = {"Content-type" : "application/x-www-form-urlencoded"};
     dynamixContextHandler.addConfiguredContextSupport("PUT", "org.ambientdynamix.contextplugins.samplepluginid, "org.ambientdynamix.contextplugins.samplecontexttype",
     {pluginVersion : '2.0.0.1', callback : configuredRequestCallback, params : params, headers : headerObject});
     **/
    this.configuredContextRequest = function (method, pluginId, contextType, optParams) {
        makeConfiguredRequest("configuredcontextrequest", method, pluginId, contextType, optParams);
    };

    /**
     Allows the web clients to add configured context support.

     @param {String} method The Dynamix Framework supports GET, PUT, POST and DELETE Methods.
     @param {String} pluginId The id of the plugin
     @param {String} contextType The context type.
     @param {Object} optParams The optional parameters.
     @example
     var paramsObject = {color : "red", lux : 22};
     var headerObject = {"Content-type" : "application/x-www-form-urlencoded"};
     dynamixContextHandler.addConfiguredContextSupport("PUT", "org.ambientdynamix.contextplugins.samplepluginid", "org.ambientdynamix.contextplugins.samplecontexttype",
     {pluginVersion : '2.0.0.1', callback : configuredRequestCallback, listener : configuredRequestListener, params : paramsObject, headers : headerObject});
     **/
    this.addConfiguredContextSupport = function (method, pluginId, contextType, optParams) {
        makeConfiguredRequest("addconfiguredcontextsupport", method, pluginId, contextType, optParams);
    };


    /**
     Get the context support information currently associated with this handler.
     @example
     dynamixContextHandler.getContextSupportInfo();
     */
    this.getContextSupportInfo = function (callback) {
        var callbackId = Utils.generateGuid();
        Dynamix.Callbacks[callbackId] = callback;
        var endpointParamsString = "getcontextsupport?contextHandlerId=" + this.id + "&callbackId=" + callbackId;
        WebClient.sendToDynamixInstance(endpointParamsString);
    };


    /**
     Remove context support for the given context type.
     @param {String} contextType The contextType for which the support should be removed.
     @param {Object} optParams The optional parameters. The web client can provide an optional callback.
     @example
     var disableVoiceControlPluginCallback = function(status, result) {
        switch(status) {
            case Dynamix.Enums.FAILURE :
                break;
            case Dynamix.Enums.SUCCESS :
                break;
        }
    };
     dynamixContextHandler.removeContextSupportForContextType("org.ambientdynamix.contextplugins.speechtotext.voiceresults",
     {callback : disableVoiceControlPluginCallback });
     **/
    this.removeContextSupportForContextType = function (contextType, optParams) {
        try {
            var endpointParamsString = "removecontextsupportforcontexttype?contextHandlerId=" + this.id + "&contextType=" + contextType;
            if (typeof optParams !== 'undefined' && typeof optParams.callback !== 'undefined') {
                var callbackId = Utils.generateGuid();
                Dynamix.Callbacks[callbackId] = optParams.callback;
                endpointParamsString = endpointParamsString + "&callbackId=" + callbackId;
            }
            WebClient.sendToDynamixInstance(endpointParamsString);
        } catch (e) {
            console.log("Error removing context support for type : " + contextType + " : " + e);
        }
    };

    /**
     Remove context support for the given context type.
     @param {String} supportId The supportId for which the support should be removed.
     @param {Object} optParams The optional parameters. The web client can provide an optional callback.
     @example
     var removeVoiceControlSupportCallback = function(status, result) {
        switch(status) {
            case Dynamix.Enums.FAILURE :
                break;
            case Dynamix.Enums.SUCCESS :
                break;
        }
    };
     dynamixContextHandler.removeContextSupportForSupportId("org.ambientdynamix.contextplugins.speechtotext",
     {callback : removeVoiceControlSupportCallback });
     **/
    this.removeContextSupportForSupportId = function (supportId, optParams) {
        try {
            var endpointParamsString = "removecontextsupportforsupportid?contextHandlerId=" + this.id + "&supportId=" + supportId;
            if (typeof optParams !== 'undefined' && typeof optParams.callback !== 'undefined') {
                var callbackId = Utils.generateGuid();
                Dynamix.Callbacks[callbackId] = optParams.callback;
                endpointParamsString = endpointParamsString + "&callbackId=" + callbackId;
            }
            WebClient.sendToDynamixInstance(endpointParamsString);
        } catch (e) {
            console.log("Error removing context support for type : " + contextType + " : " + e);
        }
    };

    /**
     Remove all context support from the given context handler.
     @param {Object} callback
     */
    this.removeAllContextSupport = function (optParams) {
        try {
            var endpointParamsString = "removeallcontextsupport?contextHandlerId=" + this.id;
            if (typeof optParams !== 'undefined' && typeof optParams.callback !== 'undefined') {
                var callbackId = Utils.generateGuid();
                Dynamix.Callbacks[callbackId] = optParams.callback;
                endpointParamsString = endpointParamsString + "&callbackId=" + callbackId;
            }
            WebClient.sendToDynamixInstance(endpointParamsString);
        } catch (e) {
            console.log("Error removing all context support : " + e.message);
        }
    };

    /**
     Open the configuration view defined by the plugin.
     @params {String} pluginId
     @params {Object} optParams optional Paramaters for the request.
     @example
     dynamixContextHandler.openContextPluginConfigurationView('org.ambientdynamix.contextplugins.batterylevel',
     {callback:callback, pluginVersion:'2.0.0.1'});
     */
    this.openContextPluginConfigurationView = function (pluginId, optParams) {
        try {
            var endpointParamsString = "opencontextpluginconfigurationview?pluginId=" + pluginId;
            if (typeof optParams !== 'undefined') {
                if (typeof optParams.callback !== 'undefined') {
                    var callbackId = Utils.generateGuid();
                    Dynamix.Callbacks["callbackId"] = optParams.callback;
                    endpointParamsString = endpointParamsString + "&callbackId=" + callbackId;
                }
                if (typeof optParams.pluginVersion !== 'undefined') {
                    endpointParamsString = endpointParamsString + "&pluginVersion=" + optParams.pluginVersion;
                }
            }
           WebClient.sendToDynamixInstance(endpointParamsString);
        }
        catch (e) {
            console.log("Error opening context plugin configuration view for plugin Id : " + pluginId);
        }
    };

    /**
     Open the default configuration view defined by the plugin.
     @params {String} pluginId
     @params {Object} optParams optional parameters for the request.
     @example
     dynamixContextHandler.openDefaultContextPluginConfigurationView('org.ambientdynamix.contextplugins.batterylevel',
     {callback:callback, pluginVersion:'2.0.0.1'});
     */
    this.openDefaultContextPluginConfigurationView = function (pluginId, optParams) {
        try {
            var endpointParamsString = "opendefaultcontextpluginconfigurationview?pluginId=" + pluginId;
            if (typeof optParams !== 'undefined') {
                if (typeof optParams.callback !== 'undefined') {
                    var callbackId = Utils.generateGuid();
                    Dynamix.Callbacks["callbackId"] = optParams.callback;
                    endpointParamsString = endpointParamsString + "&callbackId=" + callbackId;
                }
                if (typeof optParams.pluginVersion !== 'undefined') {
                    endpointParamsString = endpointParamsString + "&pluginVersion=" + optParams.pluginVersion;
                }
            }
           WebClient.sendToDynamixInstance(endpointParamsString);
        } catch (e) {
            console.log("Error opening default context plugin configuration view for plugin Id : " + pluginId);
        }
    };
}